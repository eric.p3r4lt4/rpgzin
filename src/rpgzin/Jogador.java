package rpgzin;

public class Jogador {
    private int vida, mana;
    private String nome;
    private Magia[] magias = new Magia[4];
    private Inventario inv = new Inventario();
    private int magiQunt = 0;
    private int itemQunt = 0;

    public int getItemQunt() {
        return itemQunt;
    }

    public void setItemQunt(int itemQunt) {
        this.itemQunt = itemQunt;
    }
    
    public Jogador(int vida,int mana, String nome){
        this.nome = nome;
        this.vida = vida;
        this.mana = mana;
     }
    
    public boolean executaMagia(int i){
        if(mana>=magias[i].getCusto()){
            mana -= magias[i].getCusto();
            return true;
        }
        
        else{
            return false;
        }
    }
    
    public String mostraInvent(int a){
        for(int i=a;i<(inv.getLimite());i++){
            if(!("".equals(inv.getItens(i)))){
                return inv.getItens(i);
            }
        }
        return null;
    }

    public int getMagiQunt() {
        return magiQunt;
    }

    public void setMagiQunt(int magiQunt) {
        this.magiQunt = magiQunt;
    }
    
    public boolean setInvent(String item){
        return inv.recebeItem(item);
    }
    
    public void setMagia(String nome, int manacust, int i){
        magias[i] = new Magia(nome,manacust);
    }
    
    public boolean falseRetiraItem(String item){
        return inv.retiraItem(item);
    }
    
    public String getMagiaNome(int i){
        return magias[i].getNome();
    }
    
    public int getMagiaCusto(int i){
        return magias[i].getCusto();
    }
    
    public String getNome(){
        return nome;
    }

    public int getVida() {
        return vida;
    }

    public int getMana() {
        return mana;
    }
    
    @Override
    public String toString(){
        return "nome: " + nome + "|vida: " + vida + "|mana: " + mana;
    }
}
