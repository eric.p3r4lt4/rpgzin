package rpgzin;

public class Magia {
    private String nome;
    private int custo;
    
    public Magia(String nom, int c){
        this.nome = nom;
        this.custo = c;
    }
    
    public String getNome(){
        return nome;
    }
    
    public int getCusto(){
        return custo;
    }
}
