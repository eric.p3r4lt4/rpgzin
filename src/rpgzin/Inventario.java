package rpgzin;

public class Inventario {
    int i;
    private int limite = 10;
    private String[] itens = new String[limite];
    private String[] equip = new String[5];

    public Inventario() {
        for(i=0;i<limite;i++){
            itens[i] = " ";
        }
        
        equip[0] = "Elmo";
        equip[1] = "Peitoral";
        equip[2] = "Calças";
        equip[3] = "Botas";
        equip[4] = "Armitcha";
    }
    
    public boolean recebeItem(String item){
        for(i=0;i<limite;i++){
            if("".equals(itens[i])){
                itens[i]=item;
                i = limite;
                return true;
            }
            
            else if(i==(limite-1)){
                return false;
            }
        }
        return false;
    }
    
    public boolean retiraItem(String item){
        for(i=0;i<limite;i++){
            if(itens[i].equals(item)){
                itens[i] = "";
                i=limite;
                return true;
            }
            
            else if (i == (limite-1)){
                return false;
            }
        }
        return false;
    }

    public String getItens(int i) {
        return itens[i];
    }
    
    public int getLimite(){
        return limite;
    }
}
