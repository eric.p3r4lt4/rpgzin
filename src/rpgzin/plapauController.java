/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgzin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Cliente
 */
public class plapauController implements Initializable {
    Jogador p1, p2;
    int p=1;
    
    //Label
    @FXML
    private Label retorno;
    @FXML
    private Label persoLabel;
    @FXML
    private Label ggetNome;
    @FXML
    private Label ggetVida;
    @FXML
    private Label ggetMana;
    @FXML
    private Label statusVida;
    @FXML
    private Label statusMana;
    @FXML
    private Label addNome;
    @FXML
    private Label addNomeMagia;
    @FXML
    private Label addCusto;
    @FXML
    private Label nomeItem;
    @FXML
    private Label invItem;
    
    //TextField
    @FXML
    private TextField ggetNomeText;
    @FXML
    private TextField ggetVidaText;
    @FXML
    private TextField ggetManaText;
    @FXML
    private TextField statusNome;
    @FXML
    private TextField addNomeText;
    @FXML
    private TextField addNomeMagiaText;
    @FXML
    private TextField addCustoText;
    @FXML
    private TextField nomeItemText;
    
    //Button
    @FXML
    private Button getPerso;
    @FXML
    private Button status;
    @FXML
    private Button addMagia;
    @FXML
    private Button executaMagia;
    @FXML
    private Button adicionar;
    @FXML
    private Button retirar;
    @FXML
    private Button inventario;
    @FXML
    private Button statusGetNome;
    @FXML
    private Button statusSair;
    @FXML
    private Button getNovaMagia;
    @FXML
    private Button getUsarMagia;
    @FXML
    private Button addItem;
    @FXML
    private Button retiItem;
    @FXML
    private Button proximoSair;
    
    //Métodos
    @FXML
    private void getPersoButton(){
        String name = ggetNomeText.getText();
        int life = Integer.parseInt(ggetVidaText.getText());
        int Mana = Integer.parseInt(ggetManaText.getText());
        
        if(p==1){
            p1 = new Jogador(life,Mana,name);
            persoLabel.setText("Personagem 2:");
            ggetNomeText.setText("");
            ggetVidaText.setText("");
            ggetManaText.setText("");
            p++;
        }
        else{
            p2 = new Jogador(life,Mana,name);
            persoLabel.setVisible(false);
            ggetNome.setVisible(false);
            ggetVida.setVisible(false);
            ggetMana.setVisible(false);
            ggetNomeText.setVisible(false);
            ggetVidaText.setVisible(false);
            ggetManaText.setVisible(false);
            getPerso.setVisible(false);
            
            status.setVisible(true);
            addMagia.setVisible(true);
            executaMagia.setVisible(true);
            adicionar.setVisible(true);
            retirar.setVisible(true);
            inventario.setVisible(true);
        }
    }
    
    @FXML
    private void statusButton(){
        retorno.setVisible(false);
        bloqueia(true);
        statusNome.setVisible(true);
        statusGetNome.setVisible(true);
    }
    
    @FXML
    private void statusGetNomeButton(){
        retorno.setVisible(false);
        String name = statusNome.getText();
        statusVida.setVisible(true);
        statusMana.setVisible(true);
        statusSair.setVisible(true);
        statusNome.setEditable(false);
        statusGetNome.setDisable(true);
        
        if(name.equals(p1.getNome())){
            statusVida.setText("Vida: " + p1.getVida());
            statusMana.setText("Mana: " + p1.getMana());
        }
        
        else if(name.equals(p2.getNome())){
            statusVida.setText("Vida: " + p2.getVida());
            statusMana.setText("Mana: " + p2.getMana());
        }
        
        else{
            statusVida.setVisible(false);
            statusMana.setVisible(false);
            statusNome.setEditable(true);
            statusNome.setVisible(false);
            statusGetNome.setDisable(false);
            statusGetNome.setVisible(false);
            
            retorno.setText("Não há um personagem com esse nome!");
        }
    }
    
    @FXML
    private void statusSairButton(){
        statusVida.setVisible(false);
        statusMana.setVisible(false);
        statusNome.setEditable(true);
        statusNome.setVisible(false);
        statusGetNome.setDisable(false);
        statusGetNome.setVisible(false);
    }
    
    @FXML
    private void addMagiaButton(){
        retorno.setVisible(false);
        bloqueia(true);
        addNomeText.setVisible(true);
        addNome.setVisible(true);
        addNomeMagia.setVisible(true);
        addNomeMagiaText.setVisible(true);
        addCusto.setVisible(true);
        addCustoText.setVisible(true);
        getNovaMagia.setVisible(true);
    }
    
    @FXML
    private void getNovaMagia(){
        String name = addNomeText.getText();
        String mNome = addNomeMagiaText.getText();
        int nCusto = Integer.parseInt(addCusto.getText());
        if(name.equals(p1.getNome())){
            p1.setMagia(mNome, nCusto, p1.getMagiQunt());
            p1.setMagiQunt(((p1.getMagiQunt())+1));
            
            if(p1.getMagiQunt()==4){
                p1.setMagiQunt(0);
            }
        }
        
        else if(name.equals(p2.getNome())){
            p2.setMagia(mNome, nCusto, p2.getMagiQunt());
            p2.setMagiQunt(((p2.getMagiQunt())+1));
            
            if(p2.getMagiQunt()==4){
                p2.setMagiQunt(0);
            }
        }
        else{
            persoNaoEncon();
        }
        
        bloqueia(false);
        addNomeText.setVisible(false);
        addNome.setVisible(false);
        addNomeMagia.setVisible(false);
        addNomeMagiaText.setVisible(false);
        addCusto.setVisible(false);
        addCustoText.setVisible(false);
        getNovaMagia.setVisible(false);
    }
    
    @FXML
    private void executaMagiaButton(){
        retorno.setVisible(false);
        bloqueia(true);
        addNomeText.setVisible(true);
        addNome.setVisible(true);
        addNomeMagia.setVisible(true);
        addNomeMagiaText.setVisible(true);
        getUsarMagia.setVisible(true);
    }
    
    @FXML
    private void getUsarMagiaButton(){
        String name = addNomeText.getText();
        String mNome = addNomeMagiaText.getText();
        retorno.setVisible(true);
        
        if(name.equals(p1.getNome())){
            for(int i=0;i<(p1.getMagiQunt());i++){
                if(p1.getMagiaNome(i).equals(mNome)){
                    if(p1.executaMagia(i)){
                        retorno.setText("-"+p1.getMagiaCusto(i)+" de mana"
                                        + "\nVocê tem \'"+p1.getMana()+"\' de "
                                        + "mana atualmente.");
                        i=4;
                    }
                    else{
                        retorno.setText("Não há mana suficiente!");
                    }
                    
                }
                
                else if(i==(p1.getMagiQunt()-1)){
                    retorno.setText("Não há nenhuma magia com esse nome");
                }
            }
        }
        
        else if(name.equals(p2.getNome())){
           for(int i=0;i<(p2.getMagiQunt());i++){
                if(p2.getMagiaNome(i).equals(mNome)){
                    if(p2.executaMagia(i)){
                        retorno.setText("-"+p2.getMagiaCusto(i)+" de mana"
                                        + "\nVocê tem \'"+p2.getMana()+"\' de "
                                        + "mana atualmente.");
                        i=4;
                    }
                    else{
                        retorno.setText("Não há mana suficiente!");
                    }
                    
                }
                
                else if(i==(p2.getMagiQunt()-1)){
                    retorno.setText("Não há nenhuma magia com esse nome");
                }
            }
        }
        else{
            persoNaoEncon();
        }
        
        bloqueia(false);
        addNomeText.setVisible(false);
        addNome.setVisible(false);
        addNomeMagia.setVisible(false);
        addNomeMagiaText.setVisible(false);
        getNovaMagia.setVisible(false);
    }
    
    @FXML
    private void adicionarButton(){
        retorno.setVisible(false);
        bloqueia(true);
        addNomeText.setVisible(true);
        addNome.setVisible(true);
        nomeItem.setVisible(true);
        nomeItemText.setVisible(true);
        addItem.setVisible(true);
    }
    
    @FXML
    private void addItemButton(){
        String name = addNomeText.getText();
        String iNome = nomeItem.getText();
        
        retorno.setVisible(true);
        if(name.equals(p1.getNome())){
            for(int i=0;i<(p1.getMagiQunt());i++){
                if(p1.setInvent(iNome)){
                    retorno.setText("Feito!");
                    p1.setItemQunt(p1.getItemQunt()+1);
                }
                
                else{
                    retorno.setText("Seu inventário está cheio!");
                }
            }
        }
        
        else if(name.equals(p2.getNome())){
            for(int i=0;i<(p2.getMagiQunt());i++){
                if(p2.setInvent(iNome)){
                    retorno.setText("Feito!");
                    p2.setItemQunt(p2.getItemQunt()+1);
                }
                
                else{
                    retorno.setText("Seu inventário está cheio!");
                }
            }
        }
        else{
            persoNaoEncon();
        }
        
        bloqueia(false);
        addNomeText.setVisible(false);
        addNome.setVisible(false);
        nomeItem.setVisible(false);
        nomeItemText.setVisible(false);
        addItem.setVisible(false);
    }
    
    @FXML
    private void retirarButton(){
        retorno.setVisible(false);
        bloqueia(true);
        addNomeText.setVisible(true);
        addNome.setVisible(true);
        nomeItem.setVisible(true);
        nomeItemText.setVisible(true);
        retiItem.setVisible(true);
    }
    
    @FXML
    private void retiItemButton(){
        String name = addNomeText.getText();
        String Nome = nomeItemText.getText();
        retorno.setVisible(true);
        
        if(name.equals(p1.getNome())){
            if(p1.falseRetiraItem(Nome)){
                retorno.setText("Item retirado com sucesso!");
            }
            
            else{
                retorno.setText("Esse item não está no inventário!");
            }
        }
        
        else if(name.equals(p2.getNome())){
           if(p2.falseRetiraItem(Nome)){
                retorno.setText("Item retirado com sucesso!");
            }
            
            else{
                retorno.setText("Esse item não está no inventário!");
            }
        }
        else{
            persoNaoEncon();
        }
        
        bloqueia(false);
        addNomeText.setVisible(false);
        addNome.setVisible(false);
        nomeItem.setVisible(false);
        nomeItemText.setVisible(false);
        retiItem.setVisible(false);
    }
    
    @FXML
    private void inventarioButton(){
        retorno.setVisible(false);
        bloqueia(true);
        addNomeText.setVisible(true);
        addNome.setVisible(true);
        proximoSair.setVisible(true);
        proximoSair.setText("Enviar");
    }
    
    int a1=0,a2=0;
    String obj;
    
    @FXML
    private void proximoSairButton(){
        invItem.setVisible(true);
        String name = addNomeText.getText();
        
        if(name.equals(p1.getNome())){
            obj = p1.mostraInvent(a1);
            if(a1<(p1.getItemQunt()-1)){
                proximoSair.setText("Próximo item");
                invItem.setText(obj);
                a1++;
            }
            
            else if(a1==(p1.getItemQunt()-1)){
                proximoSair.setText("Sair");
                invItem.setText(obj);
                a1++;
            }
            
            else{
                bloqueia(false);
                addNomeText.setVisible(false);
                addNome.setVisible(false);
                proximoSair.setVisible(false);
                
                a1=0;
            }
        }
        
        else if(name.equals(p2.getNome())){
           obj = p2.mostraInvent(a2);
           if(a2<(p2.getItemQunt()-1)){
                proximoSair.setText("Próximo item");
                invItem.setText(obj);
                a2++;
            }
            
            else if(a2==(p2.getItemQunt()-1)){
                proximoSair.setText("Sair");
                invItem.setText(obj);
                a2++;
            }
            
            else{
                bloqueia(false);
                addNomeText.setVisible(false);
                addNome.setVisible(false);
                proximoSair.setVisible(false);
                
                a2=0;
            }
        }
        else{
            persoNaoEncon();
        }
    }
    
    public void bloqueia(boolean f){
        status.setDisable(f);
        addMagia.setDisable(f);
        executaMagia.setDisable(f);
        adicionar.setDisable(f);
        retirar.setDisable(f);
        inventario.setDisable(f);
    }
    
    public void persoNaoEncon(){
        retorno.setVisible(true);
        retorno.setText("Não há um personagem com esse nome!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
